package main

import (
	"fmt"
	"io"
	"os"
	"sort"
)

const (
	noItem   = "│\t"
	item     = "├───"
	lastItem = "└───"
	tab      = "\t"
)

func dirTree(out io.Writer, path string, printFiles bool) error {
	return dirTreeWLine(out, path, printFiles, "")
}

func dirTreeWLine(out io.Writer, path string, printFiles bool, inheritedLine string) error {
	var unitsList []os.FileInfo // Список вложенных файлов и директорий
	if unit, err := os.Open(path); err == nil {
		units, _ := unit.Readdir(-1)
		for _, unit := range units {
			if unit.IsDir() || printFiles { // Если это директория ИЛИ нужны файлы
				unitsList = append(unitsList, unit)
			}
		}
		unit.Close()
	}

	// Сортировка по алфавиту
	sort.Slice(unitsList, func(i, j int) bool {
		return unitsList[i].Name() < unitsList[j].Name()
	})

	for index, unit := range unitsList {
		var last = index == len(unitsList)-1
		var branchWFileName string // Конец ветки или середина + имя файла\директории
		if last {
			branchWFileName = inheritedLine + lastItem + unit.Name()
		} else {
			branchWFileName = inheritedLine + item + unit.Name()
		}

		if unit.IsDir() {
			fmt.Fprintln(out, branchWFileName) // Вывести себя и уйти в рекурсию
			if last {
				dirTreeWLine(out, path+"/"+unit.Name(), printFiles, inheritedLine+tab)
			} else {
				dirTreeWLine(out, path+"/"+unit.Name(), printFiles, inheritedLine+noItem)
			}
		} else { // Файлы или ссылки; printFiles отфильтровано раньше
			if 0 < unit.Size() {
				fmt.Fprintf(out, "%v (%vb)\n", branchWFileName, unit.Size())
			} else {
				fmt.Fprintf(out, "%v (empty)\n", branchWFileName)
			}
		}
	}
	return nil
}
