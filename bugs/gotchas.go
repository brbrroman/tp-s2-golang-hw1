package main

import (
	"fmt"
	"sort"
	"strings"
)

func ReturnInt() int {
	var a int = 1
	return a
}

func ReturnFloat() float32 {
	var a float32 = 1.1
	return a
}

func ReturnIntArray() [3]int {
	a := [3]int{1, 3, 4}
	return a
}

func ReturnIntSlice() []int {
	slice := make([]int, 0, 3)
	for i := 1; i <= 3; i++ {
		slice = append(slice, i)
	}
	return slice
}

func IntSliceToString(input []int) string {
	res := make([]string, 0, len(input))
	for key := range input {
		res = append(res, fmt.Sprint(input[key]))
	}
	return strings.Join(res, "")
}

func MergeSlices(slice1 []float32, slice2 []int32) []int {
	slice := make([]int, 0, len(slice1)+len(slice2))
	for i := range slice1 {
		slice = append(slice, int(slice1[i]))
	}
	for i := range slice2 {
		slice = append(slice, int(slice2[i]))
	}
	return slice
}

func GetMapValuesSortedByKey(inp map[int]string) []string {
	res := make([]string, 0, len(inp))
	var keys []int
	for k := range inp {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, key := range keys {
		res = append(res, inp[key])
	}
	return res
}
