package main

import (
	"errors"
	"fmt"
	"strconv"
)

type stack []int

func (array *stack) push(val int) {
	*array = append(*array, val)
}

func (array *stack) pop() int {
	val := (*array)[len(*array)-1]
	*array = (*array)[:len(*array)-1]
	return val
}

// Калькулятор. Обратная польская запись
func Calc(exp string) (res int, err error) {
	defer func() {
		if terr := recover(); terr != nil {
			err = fmt.Errorf("Error: %v", terr)
		}
	}()

	var operands stack
	number := ""

	for _, symbol := range exp {

		switch {
		case symbol == ' ' || symbol == '\n': // Добавить число
			if number != "" {
				if val, err := strconv.Atoi(number); err == nil {
					operands.push(val)
					number = ""
				}
			}
		case symbol == '+':
			operands.push(operands.pop() + operands.pop())
		case symbol == '-':
			operands.push(-operands.pop() + operands.pop())
		case symbol == '*':
			operands.push(operands.pop() * operands.pop())
		case symbol == '/':
			rOperand := operands.pop()
			lOperand := operands.pop()
			operands.push(lOperand / rOperand)
		case '0' <= symbol && symbol <= '9':
			number += string(symbol)
		default: // Что-то левое
			return 0, errors.New("Unexpected symbol: " + string(symbol))
		}
	}
	return operands[0], nil
}
