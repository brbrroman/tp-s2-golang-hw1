package main

import "testing"

type testpair struct {
	expr     string
	expected int
}

var normalTests = []testpair{
	{"5 4 +", (5 + 4)},
	{"5 4 -", (5 - 4)},
	{"5 4 *", (5 * 4)},
	{"5 4 /", (5 / 4)},
	{"7 12 * 5 4 * / 11 + 10 / 3 *", ((7*12/(5*4) + 11) / 10 * 3)},
}

var errorTests = []testpair{
	{"15 0 /", 0},
	{"5 4 @", 0},
}

func TestCalcNormal(t *testing.T) {
	for _, pair := range normalTests {
		result, err := Calc(pair.expr)
		if (result != pair.expected) || (err != nil) {
			t.Errorf("results not match\nGot: %v\nExpected: %v", result, pair.expected)
		}
	}
}

func TestCalcErrors(t *testing.T) {
	for _, pair := range errorTests {
		_, err := Calc(pair.expr)
		if err == nil {
			t.Errorf("Wrong behavior")
		}
	}
}
